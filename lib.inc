section .text




; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    xor rdi,rdi
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    mov sil, byte[rax+rdi]
    cmp sil, 0
    je .end
    inc rax
    jmp .loop
.end:
    ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length	; в rax лежит длина строки
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, rsp
    mov r9, 10
    push 0
.loop:
    mov rdx, 0
    div r9
    add rdx, 48
    dec rsp
    mov byte[rsp], dl
    cmp rax, 0
    ja .loop
    mov rdi, rsp
    push r8
    call print_string
    pop r8
    mov rsp, r8
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jns .pos
.neg:
    neg rdi
    mov r10, rdi
    mov rdi, 0x2D
    call print_char
    mov rdi, r10
.pos:
    call print_uint
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
.loop:
    mov r9b, byte[r8 + rdi]
    mov r10b, byte[r8 + rsi]
    cmp r9b, r10b
    jne .ret_zero
    cmp r9b, 0
    je .ret_one
    inc r8
    jmp .loop
.ret_zero:
    mov rax, 0
    ret
.ret_one:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret




; Принимает: адрес начала буфера, размер буфера
; Принимает: адрес начала буфера(rdi), размер буфера(rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
     push rdi
     push r9
     mov r9, rdi
     push r10
     mov r10, rsi
.probel:
     call read_char
     cmp rax, 0x20
     je .probel
     cmp rax, 0x09
     je .probel
     cmp rax, 0xA
     je .probel
.loop:
     cmp rax, 0x0
     je .success
     cmp rax, 0x20
     je .success
     cmp rax, 0x9
     je .success
     cmp rax, 0xA
     je .success
     dec r10
     cmp r10, 0
     jbe .too_long
     mov byte [r9], al
     inc r9
     call read_char
     jmp .loop

.success:
     mov byte [r9], 0
     pop r9
     pop r10
     mov rdi, [rsp]
     call string_length
     mov rdx, rax
     pop rax
     ret

.too_long:
     pop r9
     pop r10
     pop rdi
     mov rax, 0
     ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r10, r10
    mov rcx, 10
    xor rax, rax
.loop:
    xor r11, r11
    mov r11b, byte[rdi + r10]
    cmp r11b, 0x39
    ja .end
    cmp r11b, 0x30
    jb .end
    inc r10
    sub r11b, '0'
    mul rcx
    add rax, r11
    jmp .loop
.end:
    mov rdx, r10
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '+'
    je .parse
    cmp byte[rdi], '-'
    jne parse_uint
.parse:
    push rdi             
    inc rdi
    call parse_uint
    pop rdi
    inc rdx
    cmp byte[rdi], '+'
    je .return
    neg rax
    .return:
    ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Принимает указатель на строку(rdi), указатель на буфер(rsi) и длину буфера(rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r8, r8
.loop:
    cmp r8, rdx
    je .bad_end
    mov rax, [rdi + r8]
    cmp rax, 0
    je .end
    mov [rsi+r8], rax
    inc r8
    jmp .loop
.bad_end:
    mov rax, 0
    ret
.end:
    mov rax, r8
    ret

